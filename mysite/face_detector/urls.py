from django.urls import path

from . import views

app_name = 'face_detector'
urlpatterns = [
    # ex: /face_detector/
    path('', views.index, name='index'),

    path('detect/', views.detect, name='detect'),

    path('register/', views.register, name='register'),

    path('login/', views.loginUser, name='login'),

]
