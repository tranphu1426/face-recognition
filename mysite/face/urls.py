from django.urls import path

from . import views

app_name = 'face'
urlpatterns = [
    # ex: /face/
    path('', views.index, name='index'),
    # ex: /face/5/
    path('<int:department_id>/', views.detail, name='detail'),

    path('register/', views.register, name='register'),

    path('<int:people_id>/info/', views.info, name='info'),
]
