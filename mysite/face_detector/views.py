from django.http.response import HttpResponse
from django.shortcuts import render

# import the necessary packages
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import numpy as np
import urllib
import json
import cv2
import os
import ssl
from PIL import Image

from face_detector.models import Department, People

# define the path to the face detector
FACE_DETECTOR_PATH = os.path.join(os.path.dirname(__file__), 'static', 'face_detector' , 'images')
FACE_DETECTOR_RECOGNIZER = os.path.join(os.path.dirname(__file__), 'static', 'face_detector' , 'recognizer')

recognizer = cv2.face.LBPHFaceRecognizer_create()

# Create your views here.
def index(request):
    # return HttpResponse("Nhan dang khuong mat o day!!!")
    department_list = Department.objects.all()
    context = {'department_list': department_list}

    # full_path = os.path.join(os.path.dirname(__file__), 'static/face_detector/images')
    # print("====================================")
    # print("full_path: " + FACE_DETECTOR_PATH)
    # print(os.path.exists(FACE_DETECTOR_PATH))

    return render(request, "face_detector/index.html", context)

@csrf_exempt
def detect(request):
    # initialize the data dictionary to be returned by the request
    data = {"success": False}
    
    # check to see if this is a post request
    if request.method == "POST":
        # id = request.POST['id']
        # check to see if an image was uploaded
        if request.FILES.get("image", None) is not None:
            # grab the uploaded image
            image = _grab_image(stream=request.FILES["image"])

        # otherwise, assume that a URL was passed in
        else:
            # grab the URL from the request
            url = request.POST.get("url", None)
            # if the URL is None, then return an error
            if url is None:
                data["error"] = "No URL provided."
                return JsonResponse(data)
            # load the image and convert
            image = _grab_image(url=url)
        # convert the image to grayscale, load the face cascade detector,
        # and detect faces in the image
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # detector = cv2.CascadeClassifier(FACE_DETECTOR_PATH)
        detector = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml") 
        rects = detector.detectMultiScale(image, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

        # construct a list of bounding boxes from the detection
        rects = [(int(x), int(y), int(x + w), int(y + h))
                 for (x, y, w, h) in rects]

        # for (x, y, w, h) in rects:
        #     cv2.imwrite(FACE_DETECTOR_PATH + '/User.'+str(id)+'.'+str(id)+'.jpg',image[y: y+h,x: x+w])

        # update the data dictionary with the faces detected
        data.update({"num_faces": len(rects), "faces": rects, "success": True})
    
    print("FACE_DETECTOR_PATH: " + FACE_DETECTOR_PATH)
    print(data)
    # getDataFromWebCam()

    # return a JSON response
    return JsonResponse(data)


def _grab_image(path=None, stream=None, url=None):
    # if the path is not None, then load the image from disk
    if path is not None:
        image = cv2.imread(path)
    # otherwise, the image does not reside on disk
    else:
        # if the URL is not None, then download the image
        if url is not None:
            ssl._create_default_https_context = ssl._create_unverified_context
            resp = urllib.request.urlopen(url)
            data = resp.read()
        # if the stream is not None, then the image has been uploaded
        elif stream is not None:
            data = stream.read()
        # convert the image to a NumPy array and then read it into
        # OpenCV format
        image = np.asarray(bytearray(data), dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

@csrf_exempt
def register(request):
    # initialize the data dictionary to be returned by the request
    data = {"success": False}

    # check to see if this is a post request
    if request.method == "POST":
        # id = request.POST['id']
        name = request.POST['name']
        departmentId = request.POST['department_id']
        # check to see if an image was uploaded
        if request.FILES.get("image", None) is not None:
            # grab the uploaded image
            image = _grab_image(stream=request.FILES["image"])

        # otherwise, assume that a URL was passed in
        else:
            # grab the URL from the request
            url = request.POST.get("url", None)
            # if the URL is None, then return an error
            if url is None:
                data["error"] = "No URL provided."
                return JsonResponse(data)
            # load the image and convert
            image = _grab_image(url=url)
        # convert the image to grayscale, load the face cascade detector,
        # and detect faces in the image
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # detector = cv2.CascadeClassifier(FACE_DETECTOR_PATH)
        detector = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml") 
        rects = detector.detectMultiScale(image, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

        # construct a list of bounding boxes from the detection
        rects = [(int(x), int(y), int(x + w), int(y + h))
                 for (x, y, w, h) in rects]

        people = People(name=name, department_id=departmentId)
        people.save()
        print("==================================")
        print("people.id: " + str(people.id))
        print(people)

        for (x, y, w, h) in rects:
            cv2.imwrite(FACE_DETECTOR_PATH + '/User.'+str(people.id)+'.'+str(people.id)+'.jpg',image[y: y+h,x: x+w])

        # update the data dictionary with the faces detected
        data.update({"num_faces": len(rects), "faces": rects, "success": True})
    
    
    print(data)
    # getDataFromWebCam()

    # getImageWithId()

    faces, Ids = getImageWithId()
    recognizer.train(faces, np.array(Ids))

    # if not os.path.exists(FACE_DETECTOR_RECOGNIZER):
    recognizer.save(os.path.join(FACE_DETECTOR_RECOGNIZER, 'trainningData.yml'))

    # return a JSON response
    return JsonResponse(data)


def getDataFromWebCam():

    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml") 

    cap = cv2.VideoCapture(0)

    while (True):

        ret, frame = cap.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = face_cascade.detectMultiScale(gray)

        for (x,y,w,h) in faces:

            cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)

        cv2.imshow('DETECTING FACE', frame)

        if(cv2.waitKey(1) & 0xFF == ord('q')):
            break

    cap.release()
    cv2.destroyAllWindows()


def getImageWithId():

    #Lay duong dan tat ca cac anh
    imagePaths = [os.path.join(FACE_DETECTOR_PATH, f) for f in os.listdir(FACE_DETECTOR_PATH)]

    faces = []
    IDs = []
    print(imagePaths)

    for imagePath in imagePaths:

        faceImg = Image.open(imagePath).convert('L')

        faceNp = np.array(faceImg, 'uint8')

        # print(faceNp)
        print(imagePath)

        #Id = int(imagePath.split('\\')[1].split('.')[1])
        Id = int(imagePath.split('images\\')[1].split('.')[1])

        faces.append(faceNp)
        IDs.append(Id)

        cv2.imshow('trainning', faceNp)
        cv2.waitKey(10)

    return faces, IDs


def loginUser(request):
    return render(request, "face_detector/login.html", {})