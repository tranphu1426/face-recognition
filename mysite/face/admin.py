from django.contrib import admin
from .models import Department, People

# Register your models here.
admin.site.register(Department)
admin.site.register(People)