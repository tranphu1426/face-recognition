from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.urls.base import reverse

from face.models import Department, People

# Create your views here.
# def index(request):
#     return HttpResponse("Hello, world. You're at the polls index.")


def index(request):
    department_list = Department.objects.all()
    context = {'department_list': department_list}
    return render(request, "face/index.html", context)


def detail(request, department_id):
    return HttpResponse("You're looking at deparment %s." % department_id)


def register(request):
    name = request.POST['name']
    departmentId = request.POST['department_id']
    people = People(name=name, department_id=departmentId)
    people.save()

    return HttpResponseRedirect(reverse('face:info', args=(people.id,)))


def info(request, people_id):
    people = get_object_or_404(People, pk=people_id)
    try:        
        department = Department.objects.get(id=people.department_id)
    except(KeyError, People.DoesNotExist):
        print("================================================")
        # Redisplay the question voting form.
        return render(request, 'face/info.html', {
            'error_message': "This user is not exist",
        })
    else:
        return render(request, 'face/info.html', {'people': people, 'department': department})


